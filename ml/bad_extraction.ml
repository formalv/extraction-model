
type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

type reflect =
| ReflectT
| ReflectF

(** val iffP : bool -> reflect -> reflect **)

let iffP _ pb =
  let _evar_0_ = fun _ _ _ -> ReflectT in
  let _evar_0_0 = fun _ _ _ -> ReflectF in
  (match pb with
   | ReflectT -> _evar_0_ __ __ __
   | ReflectF -> _evar_0_0 __ __ __)

(** val idP : bool -> reflect **)

let idP = function
| true -> ReflectT
| false -> ReflectF

type 't pred = 't -> bool

type 't predType =
  __ -> 't pred
  (* singleton inductive, whose constructor was PredType *)

type 't pred_sort = __

type 't rel = 't -> 't pred

type 't mem_pred = 't pred
  (* singleton inductive, whose constructor was Mem *)

(** val pred_of_mem : 'a1 mem_pred -> 'a1 pred_sort **)

let pred_of_mem mp =
  Obj.magic mp

(** val in_mem : 'a1 -> 'a1 mem_pred -> bool **)

let in_mem x mp =
  Obj.magic pred_of_mem mp x

(** val mem : 'a1 predType -> 'a1 pred_sort -> 'a1 mem_pred **)

let mem pT =
  pT

(** val predType0 : ('a2 -> 'a1 pred) -> 'a1 predType **)

let predType0 x =
  Obj.magic x

module Equality =
 struct
  type 't axiom = 't -> 't -> reflect

  type 't mixin_of = { op : 't rel; mixin_of__1 : 't axiom }

  (** val op : 'a1 mixin_of -> 'a1 rel **)

  let op m =
    m.op

  type coq_type =
    __ mixin_of
    (* singleton inductive, whose constructor was Pack *)

  type sort = __

  (** val coq_class : coq_type -> sort mixin_of **)

  let coq_class cT =
    cT
 end

(** val eq_op : Equality.coq_type -> Equality.sort rel **)

let eq_op t =
  (Equality.coq_class t).Equality.op

type 't subType = { val0 : (__ -> 't); sub : ('t -> __ -> __);
                    subType__2 : (__ -> ('t -> __ -> __) -> __ -> __) }

(** val sig_subType : 'a1 pred -> 'a1 subType **)

let sig_subType _ =
  { val0 = (Obj.magic (fun e -> e)); sub = (Obj.magic (fun x _ -> x));
    subType__2 = (fun _ k_S u -> k_S (Obj.magic u) __) }

(** val mem_seq :
    Equality.coq_type -> Equality.sort list -> Equality.sort -> bool **)

let rec mem_seq t = function
| [] -> (fun _ -> false)
| y :: s' -> let p = mem_seq t s' in (fun x -> (||) (eq_op t x y) (p x))

type seq_eqclass = Equality.sort list

(** val pred_of_seq :
    Equality.coq_type -> seq_eqclass -> Equality.sort pred_sort **)

let pred_of_seq t s =
  Obj.magic mem_seq t s

(** val seq_predType : Equality.coq_type -> Equality.sort predType **)

let seq_predType t =
  predType0 (Obj.magic pred_of_seq t)

(** val path : 'a1 rel -> 'a1 -> 'a1 list -> bool **)

let rec path e x = function
| [] -> true
| y :: p' -> (&&) (e x y) (path e y p')

(** val sorted : 'a1 rel -> 'a1 list -> bool **)

let sorted e = function
| [] -> true
| x :: s' -> path e x s'

(** val merge : 'a1 rel -> 'a1 list -> 'a1 list -> 'a1 list **)

let rec merge leT s1 = match s1 with
| [] -> (fun x -> x)
| x1 :: s1' ->
  let rec merge_s1 s2 = match s2 with
  | [] -> s1
  | x2 :: s2' ->
    if leT x1 x2 then x1 :: (merge leT s1' s2) else x2 :: (merge_s1 s2')
  in merge_s1

(** val merge_sort_push :
    'a1 rel -> 'a1 list -> 'a1 list list -> 'a1 list list **)

let rec merge_sort_push leT s1 ss = match ss with
| [] -> s1 :: ss
| s2 :: ss' ->
  (match s2 with
   | [] -> s1 :: ss'
   | _ :: _ -> [] :: (merge_sort_push leT (merge leT s2 s1) ss'))

(** val merge_sort_pop : 'a1 rel -> 'a1 list -> 'a1 list list -> 'a1 list **)

let rec merge_sort_pop leT s1 = function
| [] -> s1
| s2 :: ss' -> merge_sort_pop leT (merge leT s2 s1) ss'

(** val merge_sort_rec : 'a1 rel -> 'a1 list list -> 'a1 list -> 'a1 list **)

let rec merge_sort_rec leT ss s = match s with
| [] -> merge_sort_pop leT s ss
| x1 :: l ->
  (match l with
   | [] -> merge_sort_pop leT s ss
   | x2 :: s' ->
     let s1 = if leT x1 x2 then x1 :: (x2 :: []) else x2 :: (x1 :: []) in
     merge_sort_rec leT (merge_sort_push leT s1 ss) s')

(** val sort0 : 'a1 rel -> 'a1 list -> 'a1 list **)

let sort0 leT =
  merge_sort_rec leT []

(** val eqb : Uint63.t -> Uint63.t -> bool **)

let eqb = Uint63.equal

(** val leb : Uint63.t -> Uint63.t -> bool **)

let leb = Uint63.le

(** val eqbP : Uint63.t Equality.axiom **)

let eqbP x y =
  iffP (eqb x y) (idP (eqb x y))

(** val uint_eqMixin : Uint63.t Equality.mixin_of **)

let uint_eqMixin =
  { Equality.op = eqb; Equality.mixin_of__1 = eqbP }

(** val uint_eqType : Equality.coq_type **)

let uint_eqType =
  Obj.magic uint_eqMixin

(** val append1_and_sort : Uint63.t list -> Uint63.t -> Uint63.t list **)

let append1_and_sort s x =
  if in_mem (Obj.magic x) (mem (seq_predType uint_eqType) (Obj.magic s))
  then sort0 leb s
  else sort0 leb (x :: s)

(** val append1_sorted : Uint63.t list -> Uint63.t -> Uint63.t list **)

let append1_sorted s x =
  if in_mem (Obj.magic x)
       (mem (seq_predType uint_eqType)
         ((sig_subType (Obj.magic sorted leb)).val0 (Obj.magic s)))
  then (sig_subType (sorted leb)).val0 (Obj.magic s)
  else merge leb (x :: []) ((sig_subType (sorted leb)).val0 (Obj.magic s))
