# -*- Makefile -*-

######################################################################
# USAGE:                                                             #
#                                                                    #
# make all: Build the FormalV library entirely,                      #
# make only TGTS="...vo": Build the selected libraries of FormalV.   #
#                                                                    #
# The rules this-config::, this-build::, this-only::,                #
# this-distclean::, pre-makefile::, this-clean::                     #
# and __always__:: may be extended.                                  #
#                                                                    #
# Additionally, the following variables may be customized:           #
SUBDIRS?=
COQBIN?=$(dir $(shell which coqtop))
COQMAKEFILE?=$(COQBIN)coq_makefile
COQDEP?=$(COQBIN)coqdep
COQPROJECT?=_CoqProject
COQMAKEOPTIONS?=
COQMAKEFILEOPTIONS?=
V?=
VERBOSE?=V
TGTS?=
######################################################################

# local context: -----------------------------------------------------
.PHONY: all config build only clean distclean __always__ ml-build ml-clean coqdoc coqdoc-clean ocamldoc ocamldoc-clean
.SUFFIXES:

H:= $(if $(VERBOSE),,@)  # not used yet
TOP     = $(dir $(lastword $(MAKEFILE_LIST)))
COQMAKE = $(MAKE) -f Makefile.coq $(COQMAKEOPTIONS)
COQMAKE_TESTSUITE = $(MAKE) -f Makefile.test-suite.coq $(COQMAKEOPTIONS)
BRANCH_coq:= $(shell $(COQBIN)coqtop -v | head -1 | grep -E '(trunk|master)' \
	      | wc -l | sed 's/ *//g')

# coq version:
ifneq "$(BRANCH_coq)" "0"
COQVVV:= dev
else
COQVVV:=$(shell $(COQBIN)coqtop --print-version | cut -d" " -f1)
endif

COQV:= $(shell echo $(COQVVV) | cut -d"." -f1)
COQVV:= $(shell echo $(COQVVV) | cut -d"." -f1-2)

# all: ---------------------------------------------------------------
all: config build

# Makefile.coq: ------------------------------------------------------
.PHONY: pre-makefile

Makefile.coq: pre-makefile $(COQPROJECT) Makefile
	$(COQMAKEFILE) $(COQMAKEFILEOPTIONS) -f $(COQPROJECT) -o Makefile.coq

# Global config, build, clean and distclean --------------------------
config: sub-config this-config

build: sub-build this-build

only: sub-only this-only

clean: sub-clean this-clean ml-clean coqdoc-clean ocamldoc-clean

distclean: sub-distclean this-distclean

# Local config, build, clean and distclean ---------------------------
.PHONY: this-config this-build this-only this-distclean this-clean

this-config:: __always__

this-build:: this-config Makefile.coq
	+$(COQMAKE)

this-only:: this-config Makefile.coq
	+$(COQMAKE) only "TGTS=$(TGTS)"

this-distclean:: this-clean
	rm -f Makefile.coq Makefile.coq.conf

this-clean:: __always__
	@if [ -f Makefile.coq ]; then $(COQMAKE) cleanall; fi

# Install target -----------------------------------------------------
.PHONY: install

install: __always__ Makefile.coq
	$(COQMAKE) install
# counting lines of Coq code -----------------------------------------
.PHONY: count

COQFILES = $(shell grep '.v$$' $(COQPROJECT))

count:
	@coqwc $(COQFILES) | tail -1 | \
	  awk '{printf ("%d (spec=%d+proof=%d)\n", $$1+$$2, $$1, $$2)}'
# Additionally cleaning backup (*~) files ----------------------------
this-distclean::
	rm -f $(shell find . -name '*~')

# Make in SUBDIRS ----------------------------------------------------
ifdef SUBDIRS
sub-%: __always__
	@set -e; for d in $(SUBDIRS); do +$(MAKE) -C $$d $(@:sub-%=%); done
else
sub-%: __always__
	@true
endif

# Make of individual .vo ---------------------------------------------
%.vo: __always__ Makefile.coq
	+$(COQMAKE) $@

# Make of the ml extraction ------------------------------------------
ml-build: ml/FVTM.cmx

ml/FVTM.mli: theories/time/fvtm_extraction.vo

ml/FVTM.ml: theories/time/fvtm_extraction.vo

ml/uint63.cmx: ml/uint63.mli ml/uint63.ml
	ocamlopt -I ml -c ml/uint63.mli ml/uint63.ml

ml/FVTM.cmx: ml/uint63.cmx ml/FVTM.mli ml/FVTM.ml
	ocamlopt -I ml -c ml/uint63.mli ml/FVTM.mli \
    ml/FVTM.ml

ml-clean::
	rm -f \
		ml/uint63.cmi \
		ml/uint63.cmo \
		ml/uint63.cmx \
		ml/uint63.cmxa \
		ml/uint63.cmxs \
		ml/uint63.o \
		ml/uint63.mli.d \
		ml/uint63.ml.d \
		ml/FVTM.mli \
		ml/FVTM.ml \
		ml/FVTM.cmi \
		ml/FVTM.cmo \
		ml/FVTM.cmx \
		ml/FVTM.cmxa \
		ml/FVTM.cmxs \
		ml/FVTM.o \
		ml/FVTM.mli.d \
		ml/FVTM.ml.d \
	2> /dev/null

# Documentation -----------------------------------------------------
coqdoc-clean::
	rm -rf docs/coqdoc

coqdoc: Makefile.coq
	@+$(MAKE) --no-print-directory -f Makefile.coq coqdoc

ocamldoc-inter: ml/docsrc/uint63.mli ml/docsrc/FVTM.mli
	ocamlc -c ml/docsrc/uint63.mli
	ocamlc -c -I ml/docsrc ml/docsrc/FVTM.mli

ocamldoc: ml/docsrc/FVTM.mli ocamldoc-inter
	mkdir -p docs/ocamldoc
	ocamldoc -html -d docs/ocamldoc -I ml/docsrc ml/docsrc/FVTM.mli
	ocamldoc -latex -o docs/ocamldoc/ocamldoc.tex -I ml/docsrc ml/docsrc/FVTM.mli

ocamldoc-clean::
	rm -rf docs/ocamldoc
