Require Import original.
Require Extraction ExtrOcamlBasic ExtrOCamlInt63.

Set Warnings "-extraction-opaque-accessed".
Extraction "ml/bad_extraction" append1_and_sort append1_sorted.
Set Warnings "extraction-opaque-accessed".
